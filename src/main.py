import vehicle as vh
import numpy as np
from visualization import export_animate_html as eani
from visualization import traffic_jam_vis as tj_vis
import pandas as pd


def setup():
    delta_x = 0.11
    num_cars = 20

    car1 = vh.SlowCar(delta_x*num_cars, stop_time=0.00, stop_period=0.0004)

    cars = [car1]
    for i in range (num_cars-1):
        dist = delta_x * (num_cars-1-i)
        cars.append(vh.Car(dist,car_in_front=cars[i]))

    return cars


def main_loop(delta_t, max_time, vehicles):

    data = []

    for i, t in enumerate(np.arange(0.0, max_time, delta_t)):
        for vehicle in vehicles:
            vehicle.decide(t)
        for vehicle in vehicles:
            vehicle.update(delta_t)
        vh.Car.add_to_data(i, t, vehicles, data)

    data_frame = vh.Car.list_to_frame(data)
    return data_frame


cars = setup()
data_frame = main_loop(0.00005, 0.03, cars)
data_frame.to_csv("../output/car_data.csv")
eani.export_cars_animate(data_frame, "../output/anim.html")
eani.plt_distance(data_frame, "../output/plot.png")
tj_vis.make_visualization(data_frame, 0, 600,"../output/anim_double.html")


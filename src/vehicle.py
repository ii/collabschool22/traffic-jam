"""
Provides the vehicle classes for traffic lane simulations

Classes:
    Car

"""
from enum import Enum
import pandas as pd


class State(Enum):
    FREE = 1  # car accelerates up to top speed
    AVOID = 2  # car slows down to avoid collision
    BREAK = 3  # car breaks hard
    STABLE = 4  # car maintains speed


class Car:
    def __init__(
        self,
        initial_position,
        max_speed=130.0,  # km/h
        max_acc=50000.0,  # km/h/h
        max_decc=-100000.0,  # km/h/h
        break_decc=-10000000.0,  # km/h/h
        length=0.003,  # km
        initial_speed=130.0,  # km/h
        crit_dist=0.1,  # km
        stop_dist=0.01,  # km
        car_in_front=None,
    ):
        self._max_speed = max_speed
        self._max_acc = max_acc
        self._max_decc = max_decc
        self._break_decc = break_decc
        self.length = length
        self.speed = initial_speed
        self.position = initial_position
        self._crit_dist = crit_dist
        self._stop_dist = stop_dist
        self._acc = 0.0
        self._state = State.STABLE
        self._car_in_front = car_in_front
        self.car_initialize_check()

    def car_initialize_check(self):
        if self._max_speed < 0 :
            print(self._max_speed)
            raise ValueError("""Max speed of a car is defined as a negetive value, 
                                should be a positive value,
                                defined in km per hour""")
        if self._max_acc < 0:
            raise ValueError ("maximum acceleration should be bigger than 0")
        if self._max_decc > 0:
            raise ValueError ("maximum decceleration should be a negative value")
        if self._break_decc > 0:
            raise ValueError ("break deccelaration should be a negative value")
        if self.length <= 0:
            raise ValueError("Length of a car should be a positive number, and defied in km")
        if self.speed < 0:
            raise ValueError("Speed should be a positive")
        if self._crit_dist < 0:
            raise ValueError ("critical distance should be bigger than 0")
        if self._stop_dist < 0:
            raise ValueError ("stop distance should be bigger than 0")
        



    def decide(self, t):
        self._ai(t)
        self._state_resolve()

    def _ai(self, t):
        if self._car_in_front == None:
            self._state = State.FREE
        else:
            distance = (
                self._car_in_front.position
                - self.position
                - 0.5 * self.length
                - 0.5 * self._car_in_front.length
            )
            if distance <= 0.0:
                raise ValueError(f"two cars have crashed at distance {distance}")
            elif distance < self._stop_dist:
                self._state = State.BREAK
            elif distance < self._crit_dist:
                self._state = State.AVOID
            else:
                self._state = State.FREE

    def _state_resolve(self):
        if self._state == State.FREE:
            self._acc = self._max_acc
        elif self._state == State.BREAK:
            self._acc = self._break_decc
        elif self._state == State.AVOID:
            self._acc = self._max_decc
        elif self._state == State.STABLE:
            self._acc = 0.0
        else:
            raise (ValueError("state not implimented"))

    def update(self, delta_t):
        self.speed = max(min(self._max_speed, self.speed + self._acc * delta_t), 0.0)
        self.position = self.position + self.speed * delta_t

    @staticmethod
    def add_to_data(frame, time, vehicles, data):
        for i,vehicle in enumerate(vehicles):
            data.append([frame,time,vehicle.position,0,i])

    @staticmethod
    def list_to_frame(data):
        return pd.DataFrame(data,columns=["frame", "time", "x", "y", "car_id"])

    @staticmethod
    def create_dataframe():
        return pd.DataFrame(columns=["frame", "time", "x", "y", "car_id"])


class SlowCar(Car):
    def __init__(
        self,
        initial_position,
        **kwargs
    ):
        self.stop_time = kwargs.get('stop_time',0.002)
        self.stop_period = kwargs.get('stop_period',0.00005)

        del kwargs['stop_time']
        del kwargs['stop_period']

        super().__init__(
            initial_position,
            **kwargs
        )

    def _ai(self, t):
        super()._ai(t)
        if t >= self.stop_time and t < self.stop_time + self.stop_period:
            self._state = State.AVOID

import matplotlib.animation as ani
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px


def get_postion_df(start, frame, velocity, lane, car_id):
    frame = np.arange(0, frame)
    t = frame.copy()
    x = (velocity * frame) + start
    y = [lane] * len(frame)
    car_id = [car_id] * len(frame)
    df = pd.DataFrame(zip(frame, t, x, y, car_id),
                      columns=['frame', 't', 'x', 'y', 'id'])
    return df


def create_sample_car_df(num_car):
    car_list = [get_postion_df(start=(0+(i*10)),
                               frame=20,
                               velocity=10,
                               lane=1,
                               car_id=i)
                for i in range(num_car)]
    cars_df = pd.concat(car_list)
    return cars_df


def export_cars_animate(cars_df, export_file):
    max_x = max(cars_df['x'])
    fig = plt.figure()
    fig = px.scatter(cars_df, x="x", y="y", animation_frame="frame",
                     animation_group="car_id",
                     color="car_id",
                     range_x=[0, max_x], range_y=[-1, 1])
    fig.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 0.1
    fig.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 0.1

    #fig.show()
    fig.write_html(export_file)


def plt_distance(cars_df, output_file):
    df = cars_df.pivot(index='time',
                       columns='car_id',
                       values='x')
    df.plot()
    plt.ylabel('Distance')
    plt.savefig(output_file)


#cars4_df = create_sample_car_df(4)
#export_cars_animate(cars4_df, 'car_aminate.html')
#plt_distance(cars4_df, 'distance.png')

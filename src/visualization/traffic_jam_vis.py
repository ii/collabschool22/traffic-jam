import plotly
plotly.__version__
'4.1.0'
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
import numpy as np
import pandas as pd
from colour import Color

def get_postion_df(start, frame, velocity, lane, car_id):
    dframe = np.arange(0,frame)
    x = (velocity * dframe) + start
    y = [lane] * len(dframe)
    car_id = [car_id] * len(dframe)
    df = pd.DataFrame(zip(x,y,dframe,car_id), columns=['x','y','frame','id'])
    return df

start = 0
frame = 5
velocity = 10
lane = 1
car_id = 1
x2 = []
y2 = []
#SAMPLE DATA
car1 = get_postion_df(0, frame, 10, 1, 1)
car2 = get_postion_df(15, frame, 10, 1, 2)
car3 = get_postion_df(25, frame, 10, 1, 3)
car4 = get_postion_df(35, frame, 10, 1, 4)
traffic_data = pd.concat([car1,car2,car3,car4])
max_xaxis = traffic_data['x'].max()

#TESTING
#np.array((traffic_data[(traffic_data['frame']==k)])['x'])
# a = ((traffic_data[(traffic_data['frame']==2)])['x']).to_list()
# x2.extend(a)
# x2.extend([3,4, 5])
# print(x2)
#print(traffic_data)
#print(traffic_data['x'].max())

# print(x2, y2)
#     print(np.array((traffic_data[(traffic_data['frame']==k)])['x']))
#     print("--------------------------------------------")
#     print(np.array((traffic_data[(traffic_data['frame']==k)])['frame']))
def get_x2value(traffic_data, k):
    x2.extend(((traffic_data[(traffic_data['frame']==k)])['frame']).to_list())
    #print('-----------------------------------')
    #print('x:', x2)
    return x2
def get_y2value(traffic_data, k):
    y2.extend(((traffic_data[(traffic_data['frame']==k)])['x']).to_list())
    #print('------------------------------')
    #print('k:',k)
    return y2

# for k in np.array(traffic_data['frame']):
#     print(get_x2value(traffic_data, k))
#     #print(get_y2value(k))

def make_visualization(traffic_data,start, frame, export_file):
    fig = make_subplots(rows=1, cols=2, subplot_titles = ('Road', 'Traces'))
    fig.add_trace(go.Scatter(
          x= np.array((traffic_data[(traffic_data['frame']==0)])['x']),
          y= np.array((traffic_data[(traffic_data['frame']==0)])['y']),
          mode = 'markers',
          hoverinfo='name',
          legendgroup= 'f1',
          line_color= 'green',
          name= 'f1',
          showlegend= True), row=1, col=1)
   
    fig.add_trace(go.Scatter(
         x = get_x2value(traffic_data, -1),
         y = get_y2value(traffic_data, -1),
         #x= np.array((traffic_data[(traffic_data['frame']==0)])['frame']),
        # y= np.array((traffic_data[(traffic_data['frame']==0)])['x']),
         name= 'f2',
         showlegend= True,
         hoverinfo= 'name',
         legendgroup= 'f2',
         mode = 'markers',
         line_color='green',
         ), row=1, col=2)
    fig.update_xaxes(range=[0, frame])
    #fig.update_layout(width=700, height=475)
  
    fig.update_xaxes(range=[0, 500]) #this line updates both yaxis, and yaxis2 range

    number_frames = frame
    # frames = [dict(
    #                 name = str(k),
    #                 # x = values of x at time frame k
    #                 data = [go.Scatter(x= np.array((traffic_data[(traffic_data['frame']==k)])['x'])), 
    #                         go.Scatter(x= np.array((traffic_data[(traffic_data['frame']==k)])['frame']),
    #                                 y = np.array((traffic_data[(traffic_data['frame']==k)])['x'])
    #                         ), 
    #                         ],
    #                 traces=[0, 1] # the elements of the list [0,1,2] give info on the traces in fig.data
    #                                         # that are updated by the above two go.Scatter instances
    #                 ) for k in np.array(traffic_data['frame'])]
 
    red = Color("red")
    colors = list(red.range_to(Color("green"),number_frames))


    frames = [dict(
                name = str(k),
                # x = values of x at time frame k
                data = [go.Scatter(x= np.array((traffic_data[(traffic_data['frame']==k)])['x'])), 
                        go.Scatter(x = get_x2value(traffic_data, k),
                                   y = get_y2value(traffic_data, k),
                                   marker_colorscale=px.colors.sequential.Viridis,
                                   fill = None
                            )
                        ],
                traces=[0, 1] # the elements of the list [0,1,2] give info on the traces in fig.data
                                        # that are updated by the above two go.Scatter instances
                ) for k in np.arange(number_frames)

                ]


    updatemenus = [dict(type='buttons',
                        buttons=[dict(label='Play',
                                    method='animate',
                                    args=[[f'{k}' for k in range(number_frames)], 
                                            dict(frame=dict(duration=50, redraw=False), 
                                                transition=dict(duration=0),
                                                easing='linear',
                                                fromcurrent=True,
                                                mode='immediate'
                                                                    )])],
                        direction= 'left', 
                        pad=dict(r= 10, t=85), 
                        showactive =True, x= 0.1, y= 0, xanchor= 'right', yanchor= 'top')
            ]
    sliders = [{'yanchor': 'top',
                'xanchor': 'left', 
                'currentvalue': {'font': {'size': 16}, 'prefix': 'Frame: ', 'visible': True, 'xanchor': 'right'},
                'transition': {'duration': 20.0, 'easing': 'linear'},
                'pad': {'b': 10, 't': 50}, 
                'len': 0.9, 'x': 0.1, 'y': 0, 
                'steps': [{'args': [[k], {'frame': {'duration': 20.0, 'easing': 'linear', 'redraw': False},
                                        'transition': {'duration': 0, 'easing': 'linear'}}], 
                        'label': k, 'method': 'animate'} for k in range(number_frames)       
                        ]}]
                        
    fig.update(frames=frames),
    fig.update_layout(updatemenus=updatemenus,
                    sliders=sliders)

    #this line updates both yaxis2 range
    fig.update_layout(yaxis2_range=[0, 8], yaxis2_autorange=False)
    fig.update_layout(xaxis2_range=[0, frame], xaxis2_autorange=False)
    fig.update_layout(xaxis_range=[0,8])
    fig.update_layout(xaxis=dict(title="x"))
    fig.update_layout(xaxis2=dict(title="t"))
    fig.update_layout(yaxis=dict(title="lane"))
    fig.update_layout(yaxis2=dict(title="x"))


    fig.show('browser')
    fig.write_html(export_file)
#for testing
#make_visualization(traffic_data,start, frame)

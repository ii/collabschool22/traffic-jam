from logging import raiseExceptions
import pytest
import vehicle as vh 

def test_crash_situation():
    car1 = vh.Car(initial_position = 2.0,)
    car2 = vh.Car(initial_position = 3.0,)
    dist = car2.position - car1.position
    min_dist = car1.length/2 + car2.length/2
    assert dist > min_dist
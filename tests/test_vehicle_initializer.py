from logging import raiseExceptions
import pytest
import vehicle as vh 

# PYTHONPATH=src python -m pytest tests/test_vehicle.py 

# @pytest.fixture
# def car1():
#     car = vh.Car(
#         initial_position = 2.0,
#         initial_speed = 131.0,
#     )
#     return car

#test initialisation of the car problems
def test_max_speed_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 2.0, max_speed = -2.0,)

def test_speed_over_the_limit_initializer():
    #evolve the car by one time step
    car1 = vh.Car(initial_position = 2.0, initial_speed = 131.0,)
    car1.update(1)
    assert car1._max_speed >= car1.speed    

def test_max_acc_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, max_acc = -20)

def test_max_decc_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, max_decc = 100)

def test_break_decc_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, break_decc = 100)

def test_length_initializer():
    with pytest.raises(Exception):
        car1 = vh.Car(initial_position = 0.0, length = -0.1)

def test_initial_speed_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, initial_speed = -0.1)


def test_crit_dist_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, crit_dist = -0.1)


def test_stop_dist_initializer():
    with pytest.raises(ValueError):
        car1 = vh.Car(initial_position = 0.0, stop_dist = -0.1)





# @pytest.mark.parametrize(
#     "test_input,expected",
#     [
#         ("3+5", 8),
#         pytest.param("1+7", 8, marks=pytest.mark.basic),
#         pytest.param("2+4", 6, marks=pytest.mark.basic, id="basic_2+4"),
#         pytest.param(
#             "6*9", 42, marks=[pytest.mark.basic, pytest.mark.xfail], id="basic_6*9"
#         ),
#     ],
# )
# def test_initializer_parametrized(car_, expected):
#     assert eval(test_input) == expected